package com.pobox.jolly.app;

import ai.timefold.solver.core.api.solver.Solver;
import ai.timefold.solver.core.api.solver.SolverFactory;
import com.pobox.jolly.domain.Feature;
import com.pobox.jolly.domain.TestCase;
import com.pobox.jolly.domain.TestPlan;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import jakarta.inject.Inject;
import jakarta.inject.Named;
import java.util.List;
import java.util.Set;

@Named
@Slf4j
public class JennyJApp {

    private CommandLineArgsHandler clah;
    private SolverFactory solverFactory;

    @Inject
    public JennyJApp(CommandLineArgsHandler clah, SolverFactory solverFactory) {
        this.clah = clah;
        this.solverFactory = solverFactory;
    }

    public static void main(String... args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(JennyJConfig.class);
        JennyJApp jennyJApp = context.getBean(JennyJApp.class);
        jennyJApp.doIt(args);
    }


    public static String toDisplayString(TestPlan testPlan) {
        StringBuilder sb = new StringBuilder("\n ");
        Set<TestCase> testcases = testPlan.getTestcases();
        for (TestCase testcase : testcases) {
            List<Feature> features = testcase.getFeatures();
            for (Feature feature : features) {
                sb.append(feature);
                sb.append(' ');
            }
            sb.append("\n ");
        }
        return sb.toString();
    }

    void doIt(String... args) {
        // Parse the Args
        clah.parse(args);
        log.debug("args retreived: n-tuple: {}, dim feature counts: {}", clah.getNTuple(), clah
                .getDimensionFeatureCounts());
        // Build the Solver
        Solver solver = solverFactory.buildSolver();
        log.trace("Solver built");
        TestPlan unsolvedTestPlan = new TestPlan(clah.getNTuple(), clah.getDimensionFeatureCounts());
        // Solve the problem
        solver.solve(unsolvedTestPlan);
        TestPlan solvedTestPlan = (TestPlan) solver.getBestSolution();
        // Display the result
        System.out.println("\nSolved test plan:\n" + toDisplayString(solvedTestPlan));
    }

}
