package com.pobox.jolly.app;

import joptsimple.ValueConversionException;
import joptsimple.ValueConverter;
import lombok.extern.slf4j.Slf4j;

import jakarta.inject.Named;

@Named
@Slf4j
class IntegerValueConverter implements ValueConverter<Integer> {
    @Override
    public Integer convert(String value) {
        try {
            return Integer.parseUnsignedInt(value);
        } catch (NumberFormatException nfe) {
            throw new ValueConversionException("unable to convert number", nfe);
        }
    }

    @Override
    public Class<Integer> valueType() {
        return Integer.class;
    }

    @Override
    public String valuePattern() {
        return null;
    }
}
