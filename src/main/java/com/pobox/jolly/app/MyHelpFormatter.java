package com.pobox.jolly.app;

import joptsimple.HelpFormatter;
import joptsimple.OptionDescriptor;
import lombok.extern.slf4j.Slf4j;

import jakarta.inject.Named;
import java.util.Map;

@Named
@Slf4j
class MyHelpFormatter implements HelpFormatter {
    @Override
    public String format(Map<String, ? extends OptionDescriptor> options) {
        return getHelpText();
    }

    private String getHelpText() {
        return """
                jennyJ:
                  Given a set of feature dimensions and withouts, produce tests
                  covering all n-tuples of features where all features come from
                  different dimensions.  For example (=, <, >, <=, >=, !=) is a
                  dimension with 6 features.  The type of the left-hand argument is
                  another dimension.  Dimensions are numbered 1..65535, in the order
                  they are listed.  Features are implicitly named a..z, A..Z.
                   3 Dimensions are given by the number of features in that dimension.
                  -h prints out these instructions.
                  -n specifies the n in n-tuple.  The default is 2 (meaning pairs).
                  -w gives withouts.  -w1b4ab says that combining the second feature
                     of the first dimension with the first or second feature of the
                     fourth dimension is disallowed.
                  -ofoo.txt reads old jenny testcases from file foo.txt and extends them.

                  The output is a testcase per line, one feature per dimension per
                  testcase, followed by the list of all allowed tuples that jenny could
                  not reach.

                  Example: jenny -n3 3 2 2 -w2b3b 5 3 -w1c3b4ace5ac 8 2 2 3 2
                  This gives ten dimensions, asks that for any three dimensions all
                  combinations of features (one feature per dimension) be covered,
                  plus it asks that certain combinations of features
                  (like (1c,3b,4c,5c)) not be covered.

                """;
    }
}
