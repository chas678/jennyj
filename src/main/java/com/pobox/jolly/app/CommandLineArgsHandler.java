package com.pobox.jolly.app;

import com.google.common.collect.Lists;
import joptsimple.HelpFormatter;
import joptsimple.OptionParser;
import joptsimple.OptionSet;
import joptsimple.OptionSpec;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import jakarta.inject.Inject;
import jakarta.inject.Named;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static java.util.Arrays.asList;

@Named
@Slf4j
public class CommandLineArgsHandler {
    @Getter
    private final List<Integer> dimensionFeatureCounts = Lists.newArrayList();
    @Getter
    private List<String> withouts = Lists.newArrayList();
    @Getter
    private Integer seed;
    @Getter
    private Integer nTuple;
    private final OptionSpec<String> oldOptSpec;
    private final OptionSpec<String> withoutsOptSpec;
    private final OptionSpec<Integer> ntupleOptSpec;
    private final OptionSpec<Integer> seedOptSpec;
    private final OptionSpec<Integer> dimsOptSpec;
    private final OptionParser parser;
    private final HelpFormatter myHelpFormatter;

    @Inject
    public CommandLineArgsHandler(OptionParser parser,
                                  HelpFormatter myHelpFormatter,
                                  IntegerValueConverter integerValueConverter) {
        this.parser = parser;
        this.myHelpFormatter = myHelpFormatter;
        oldOptSpec = this.parser.accepts("o").withRequiredArg();
        withoutsOptSpec = this.parser.accepts("w").withRequiredArg();
        ntupleOptSpec = this.parser.accepts("n").withRequiredArg().ofType(Integer.class).defaultsTo(2);
        seedOptSpec = this.parser.accepts("s").withRequiredArg().ofType(Integer.class).defaultsTo(-1);
        dimsOptSpec = this.parser.nonOptions().withValuesConvertedBy(integerValueConverter);
        this.parser.acceptsAll(asList("help", "h", "?"), "show help").forHelp();
    }

    public CommandLineArgsHandler parse(String... argv) {
        //CommandLineArgsHandler.log.debug("Args received to parse: {}", Arrays.toString(argv));
        OptionSet options = parser.parse(argv);
        if (options.has("h")) {
            showHelpAndQuit();
        }
        nTuple = ntupleOptSpec.value(options);
        if (options.nonOptionArguments().isEmpty() || (options.nonOptionArguments().size() < nTuple)) {
            throw new IllegalArgumentException("Need minimum of " + nTuple + " feature/dimension counts, " + "found: "
                    + options.nonOptionArguments().size());
        }
        dimensionFeatureCounts.addAll(dimsOptSpec.values(options));
        if (options.has("s")) {
            seed = seedOptSpec.value(options);
        }
        if (options.has("w")) {
            withouts.addAll(withoutsOptSpec.values(options));
        }
        if (options.has("o")) { // TODO: implement this
            throw new UnsupportedOperationException("Not yet implemented");
        }
        return this;
    }

    private void showHelpAndQuit() {
        parser.formatHelpWith(myHelpFormatter);
        try {
            parser.printHelpOn(System.out);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.exit(0);
    }

}
