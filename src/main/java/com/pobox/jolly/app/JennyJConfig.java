package com.pobox.jolly.app;

import joptsimple.OptionParser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "com.pobox")
@Slf4j
public class JennyJConfig {
    @Bean
    public OptionParser getOptionParser() {
        return new OptionParser();
    }

    @Bean
    public IntegerValueConverter getIntegerValueConverter() {
        return new IntegerValueConverter();
    }


}
