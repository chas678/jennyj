package com.pobox.jolly.domain;

import lombok.Value;
import lombok.extern.slf4j.Slf4j;

public record Feature(char dimension, int featureItem) {
    public String toString() {
        return String.valueOf(featureItem) + dimension;
    }
}
