package com.pobox.jolly.domain;

import com.google.common.collect.ImmutableSet;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.util.Set;

@Slf4j
@Data
public class Tuple {
    private Set<Feature> features;

    public Tuple(Set<Feature> features) {
        this.features = ImmutableSet.copyOf(features);
    }
}
