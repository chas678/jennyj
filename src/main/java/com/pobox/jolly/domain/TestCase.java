package com.pobox.jolly.domain;

import ai.timefold.solver.core.api.domain.entity.PlanningEntity;
import ai.timefold.solver.core.api.domain.variable.PlanningVariable;
import com.google.common.base.Objects;
import com.google.common.collect.ImmutableList;
import lombok.Data;
import lombok.ToString;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;

import java.util.HashSet;
import java.util.List;

@Data
@PlanningEntity
public class TestCase {
    private List<Feature> features;

    public TestCase(List<Feature> features) {
        this.features = ImmutableList.copyOf(features);
    }

    @PlanningVariable(valueRangeProviderRefs = "testcases")
    public List<Feature> getFeatures() {
        return features;
    }

    public void setFeatures(List<Feature> features) {
        this.features = ImmutableList.copyOf(features);
    }

    public boolean covered(Tuple tuple) {
        return new HashSet<>(getFeatures()).containsAll(tuple.getFeatures());
    }

}
