package com.pobox.jolly.domain;

import com.google.common.collect.Lists;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

@Slf4j
@Getter
@ToString
@EqualsAndHashCode
@AllArgsConstructor
public class Dimension {
    char id = '0';
    List<Feature> features = Lists.newArrayList();
}
