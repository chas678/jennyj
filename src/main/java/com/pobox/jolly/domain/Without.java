package com.pobox.jolly.domain;

import com.google.common.collect.ImmutableSet;
import lombok.Builder;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.util.Set;

@Slf4j
@Data
@Builder
public class Without {
    private Set<Tuple> tuples;
    public Without(Set<Tuple> tuples) {
        this.tuples = ImmutableSet.copyOf(tuples);
    }
}
