package com.pobox.jolly.domain;

import ai.timefold.solver.core.api.domain.solution.PlanningEntityCollectionProperty;
import ai.timefold.solver.core.api.domain.solution.PlanningScore;
import ai.timefold.solver.core.api.domain.solution.PlanningSolution;
import ai.timefold.solver.core.api.domain.valuerange.ValueRangeProvider;
import ai.timefold.solver.core.api.score.buildin.hardsoft.HardSoftScore;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.pobox.jolly.service.TupleGenerator;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

@Slf4j
@EqualsAndHashCode
@ToString
@PlanningSolution
public class TestPlan {
    @Setter
    private Set<TestCase> testcases;
    @Getter(lazy = true)
    private final List<List<Feature>> allTestCasesAsFeatureList = computeAllPossibleTestCasesAsFeatureLists();
    @Setter
    @Getter
    private int nTuples;
    @Setter
    @Getter
    private TupleGenerator tg;
    @Getter(lazy = true)
    private final Set<Feature> allFeatures = computeAllFeatures();
    @Getter(lazy = true)
    private final Set<Tuple> requiredTuples = tg.allRequiredTuples();
    @ValueRangeProvider(id = "testcases")
    private Set<TestCase> allPossibleTestcases;
    @PlanningScore
    private HardSoftScore score;

    public TestPlan() {
    }

    public TestPlan(int nTuples, List<Integer> dimX_FeatureCount) {
        this();
        checkArgument(nTuples > 1, "Must have 2 or more numberOfFeatureTuples");
        this.nTuples = nTuples;
        tg = new TupleGenerator(nTuples, checkNotNull(dimX_FeatureCount));
        allPossibleTestcases = computeAllPossibleTestCases();
        testcases = generateInitialSolution();
    }

    @PlanningEntityCollectionProperty
    public Set<TestCase> getTestcases() {
        return testcases;
    }

    Set<TestCase> generateInitialSolution() {
        Set<TestCase> initialState = Sets.newHashSet();
        for (TestCase testcase : allPossibleTestcases) {
            for (Tuple requiredTuple : getRequiredTuples()) {
                if (testcase.covered(requiredTuple)) {
                    initialState.add(testcase);
                    break;
                }
            }
        }
        return initialState;
    }


    public void setScore(HardSoftScore score) {
        this.score = score;
    }

    public Collection<?> getProblemFacts() {
        // nothing to add, all facts are already added by planner
        return Collections.emptySet();
    }


    public Set<TestCase> getTestCaseRangeList() {
        if (allPossibleTestcases == null) {
            allPossibleTestcases = computeAllPossibleTestCases();
        }
        return allPossibleTestcases;
    }

    public Set<Feature> allFeatures() {
        return getAllFeatures();
    }

    public Set<Feature> computeAllFeatures() {
        Set<Feature> allFeatures = Sets.newHashSet();
        for (Dimension dimension : tg.getDimensions()) {
            allFeatures.addAll(new ArrayList<>(dimension.getFeatures()));
        }
        return allFeatures;
    }

    @ValueRangeProvider(id = "allTestCases")
    public List<List<Feature>> computeAllTestCasesAsListsOfFeatures() {
        return getAllTestCasesAsFeatureList();
    }

    List<List<Feature>> computeAllPossibleTestCasesAsFeatureLists() {
        List<List<Feature>> allFeaturesList = Lists.newArrayList();
        allFeaturesList.addAll(getTestcases().stream()
                .map(TestCase::getFeatures).toList());
        return allFeaturesList;
    }

    Set<TestCase> computeAllPossibleTestCases() {
        Set<TestCase> result = Sets.newHashSet();
        List<Set<Feature>> featuresByDimension = Lists.newArrayList();
        for (Dimension dimension : tg.getDimensions()) {
            Set<Feature> featuresInDimension = Sets.newHashSet(dimension.getFeatures());
            featuresByDimension.add(featuresInDimension);
        }
        Set<List<Feature>> lists = Sets.cartesianProduct(featuresByDimension);
        result.addAll(lists.stream()
                .map(TestCase::new).toList());
        return result;
    }
}
