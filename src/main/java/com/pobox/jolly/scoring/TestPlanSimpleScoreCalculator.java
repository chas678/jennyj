package com.pobox.jolly.scoring;

import ai.timefold.solver.core.api.score.Score;
import ai.timefold.solver.core.api.score.buildin.hardsoft.HardSoftScore;
import ai.timefold.solver.core.api.score.calculator.EasyScoreCalculator;
import com.google.common.collect.Sets;
import com.pobox.jolly.domain.TestCase;
import com.pobox.jolly.domain.TestPlan;
import com.pobox.jolly.domain.Tuple;
import lombok.extern.slf4j.Slf4j;

import java.util.Set;

@Slf4j
public class TestPlanSimpleScoreCalculator implements EasyScoreCalculator<TestPlan> {

    @Override
    public Score calculateScore(TestPlan testPlan) {
        Set<Tuple> coveredTuples = Sets.newHashSet();
        for (TestCase testcase : testPlan.getTestcases()) {
            coveredTuples.addAll(testPlan.getRequiredTuples()
                    .stream()
                    .filter(testcase::covered).toList());
        }
        int hardScore = -1 * (testPlan.getRequiredTuples()
                                      .size() - coveredTuples.size());
        int softScore = -1 * testPlan.getTestcases()
                .size();
        log.info("Score: hard={}, soft={}", hardScore, softScore);
        return HardSoftScore.of(hardScore, softScore);
    }

}
