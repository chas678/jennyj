package com.pobox.jolly.solver;

import ai.timefold.solver.core.impl.heuristic.move.Move;
import ai.timefold.solver.core.impl.heuristic.selector.move.factory.MoveListFactory;
import com.google.common.collect.Lists;
import com.pobox.jolly.domain.TestPlan;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

@Slf4j
public class RemoveTestCaseMoveFactory implements MoveListFactory<TestPlan> {

    @Override
    public List<Move<TestPlan>> createMoveList(TestPlan testPlan) {
        List<Move<TestPlan>> moveList = Lists.newArrayList();
        // TODO: domain problem?
//        moveList.addAll(testPlan.getTestCaseRangeList().stream()
//                                .map(testCase ->
//                                        new RemoveTestCaseMove(testCase, testPlan, testPlan.getRequiredTuples()))
//                                .collect(Collectors.toList()));
        return moveList;
    }
}

