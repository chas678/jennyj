package com.pobox.jolly.solver;

import ai.timefold.solver.core.impl.heuristic.move.Move;
import ai.timefold.solver.core.impl.heuristic.selector.move.factory.MoveListFactory;
import com.google.common.collect.Lists;
import com.pobox.jolly.domain.TestPlan;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

@Slf4j
public class AddTestCaseMoveFactory implements MoveListFactory<TestPlan> {
    @Override
    public List<? extends Move<TestPlan>> createMoveList(TestPlan testPlan) {
        List<AddTestCaseMove> moveList = Lists.newArrayList();
        moveList.addAll(testPlan.getTestCaseRangeList()
                .stream()
                .map(testCase ->
                        new AddTestCaseMove(testCase, testPlan, testPlan.getRequiredTuples())).toList());
        return null; // TODO : compile fail?
    }

}

