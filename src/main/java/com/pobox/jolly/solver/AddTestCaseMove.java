package com.pobox.jolly.solver;

import ai.timefold.solver.core.api.score.director.ScoreDirector;
import ai.timefold.solver.core.impl.heuristic.move.Move;
import com.google.common.collect.ImmutableSet;
import com.pobox.jolly.domain.TestCase;
import com.pobox.jolly.domain.TestPlan;
import com.pobox.jolly.domain.Tuple;
import lombok.EqualsAndHashCode;
import lombok.extern.slf4j.Slf4j;

import java.util.Collection;
import java.util.Set;

@Slf4j
@EqualsAndHashCode
public class AddTestCaseMove implements Move<TestCase> {
    private TestCase testCase;
    private TestPlan testPlan;
    private Set<Tuple> requiredTuples;

    public AddTestCaseMove(TestCase testCase, TestPlan testPlan, Set<Tuple> requiredTuples) {
        this.testCase = testCase;
        this.testPlan = testPlan;
        this.requiredTuples = ImmutableSet.copyOf(requiredTuples);
    }

    @Override
    public boolean isMoveDoable(ScoreDirector<TestCase> scoreDirector) {
        boolean containsARequiredTuple = false;
        for (Tuple requiredTuple : requiredTuples) {
            if (testCase.covered(requiredTuple)) {
                containsARequiredTuple = true;
                break;
            }
        }
        boolean testCaseAlreadyThere = testPlan.getTestcases()
                .contains(testCase);
        return containsARequiredTuple && testCaseAlreadyThere;
    }

    @Override
    public Move<TestCase> doMove(ScoreDirector<TestCase> scoreDirector) {
        scoreDirector.beforeVariableChanged(testCase, "features");
        testPlan.getTestcases()
                .add(testCase);
        scoreDirector.triggerVariableListeners();
        scoreDirector.afterVariableChanged(testCase, "features");
        return new Move<>() {
            @Override
            public boolean isMoveDoable(ScoreDirector<TestCase> scoreDirector) {
                return false;
            }

            @Override
            public Move<TestCase> doMove(ScoreDirector<TestCase> scoreDirector) {
                return null;
            }
        };
    }

    @Override
    public Move<TestCase> rebase(ScoreDirector<TestCase> destinationScoreDirector) {
        return new RemoveTestCaseMove(testCase, testPlan, requiredTuples);

    }

    @Override
    public String getSimpleMoveTypeDescription() {
        return "AddTestCaseMove";
    }

    @Override
    public Collection<?> getPlanningEntities() {
        return testPlan.getTestcases();
    }

    @Override
    public Collection<?> getPlanningValues() {
        return testCase.getFeatures();
    }

    public String toString() {
        return "Added TestCase => " + testCase;
    }
}
