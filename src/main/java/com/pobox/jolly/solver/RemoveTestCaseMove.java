package com.pobox.jolly.solver;

import ai.timefold.solver.core.api.score.director.ScoreDirector;
import ai.timefold.solver.core.impl.heuristic.move.Move;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.pobox.jolly.domain.TestCase;
import com.pobox.jolly.domain.TestPlan;
import com.pobox.jolly.domain.Tuple;
import lombok.EqualsAndHashCode;
import lombok.extern.slf4j.Slf4j;

import java.util.Collection;
import java.util.Set;

@Slf4j
@EqualsAndHashCode
public class RemoveTestCaseMove implements Move {
    private TestCase testCase;
    private TestPlan testPlan;
    private Set<Tuple> requiredTuples;

    public RemoveTestCaseMove(TestCase testCase, TestPlan testPlan, Set<Tuple> requiredTuples) {
        this.testCase = testCase;
        this.testPlan = testPlan;
        this.requiredTuples = ImmutableSet.copyOf(requiredTuples);
    }

    @Override
    public Move doMove(ScoreDirector scoreDirector) {
        scoreDirector.beforeVariableChanged(testCase, "features");
        Move move = new RemoveTestCaseMove(testCase, testPlan, null); // TODO: What?
        scoreDirector.triggerVariableListeners();
        scoreDirector.afterVariableChanged(testCase, "features");
        return move;
    }

    @Override
    public Move rebase(ScoreDirector destinationScoreDirector) {
        return new AddTestCaseMove(testCase, testPlan, requiredTuples);
    }

    @Override
    public boolean isMoveDoable(ScoreDirector scoreDirector) {
        return testPlan.getTestcases()
                .contains(testPlan);
    }

    @Override
    public String getSimpleMoveTypeDescription() {
        return "RemoveTestCaseMove";
    }

    @Override
    public Collection<?> getPlanningEntities() {
        return ImmutableList.of(testCase);
    }

    @Override
    public Collection<?> getPlanningValues() {
        return testCase.getFeatures();
    }

    public String toString() {
        return "Removed TestCase => " + testCase;
    }
}
