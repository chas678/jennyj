package com.pobox.jolly.service;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.pobox.jolly.domain.Dimension;
import com.pobox.jolly.domain.Feature;
import com.pobox.jolly.domain.Tuple;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@Data
public class TupleGenerator {
    private static final char[] dimensionIds = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
    private final int nTuples;
    private final List<Integer> domainsFeatureCount;
    @Getter(lazy = true)
    private final ImmutableList<Dimension> dimensions = generateDimensions();

    @Builder
    public TupleGenerator(int nTuples, List<Integer> domainsFeatureCount) {
        Preconditions.checkNotNull(domainsFeatureCount, "domainsFeatureCount cannot be null");
        Preconditions.checkArgument((nTuples > 0) && (nTuples <= domainsFeatureCount.size()),
                "nTuples must be >0 and <= number of domains"
        );
        this.nTuples = nTuples;
        this.domainsFeatureCount = ImmutableList.copyOf(domainsFeatureCount);
    }

    @VisibleForTesting
    ImmutableList<Dimension> generateDimensions() {
        List<Dimension> result = Lists.newArrayListWithCapacity(domainsFeatureCount.size());
        int idx = 0;
        for (Integer singleDomainFeatureCount : domainsFeatureCount) {
            List<Feature> features = Lists.newArrayListWithCapacity(singleDomainFeatureCount);
            for (int i = 1; i <= singleDomainFeatureCount; i++) {
                features.add(new Feature(dimensionIds[idx], i));
            }
            result.add(new Dimension(dimensionIds[idx], features));
            idx++;
        }
        return ImmutableList.copyOf(result);
    }

    public Set<Tuple> allRequiredTuples() {
        Set<Tuple> result = Sets.newHashSet();
        Set<Set<Dimension>> dimensionSet = select_N_Dimensions();
        for (Set<Dimension> set : dimensionSet) {
            result.addAll(createCombinationTuples(set));
        }
        return result;
    }

    Set<Tuple> createCombinationTuples(Set<Dimension> dimensions) {
        List<Set<Feature>> input = dimensions.stream()
                                             .map(dimension -> new HashSet<>(dimension.getFeatures()))
                                             .collect(Collectors.toList());
        List<Tuple> result = Lists.newArrayList();
        result.addAll(Sets.cartesianProduct(input)
                .stream()
                .map(features -> new Tuple(Sets.newHashSet(features))).toList());
        log.debug("Cartesian Product of Dimensions is: {}", result);
        return ImmutableSet.copyOf(result);
    }

    Set<Set<Dimension>> select_N_Dimensions() {
        Set<Set<Dimension>> result = Sets.newHashSet();
        long k = (long) nTuples;
        long n = (long) getDimensions().size();

        // Gosper's Hack - http://en.wikipedia.org/wiki/Combinatorial_number_system#Applications
        for (int x = (1 << k) - 1; (x >>> n) == 0; x = nextCombo(x)) {
            String comboString = StringUtils.leftPad(Integer.toBinaryString(x), getDimensions().size(), '0');
            log.debug("Dimension set selection (Binary): {}", comboString);
            result.add(getSelectedDimensionsFromBinaryString(comboString));
        }
        return ImmutableSet.copyOf(result);
    }

    int nextCombo(int x) {
        // moves to the next combination with the same number of 1 bits
        int u = x & -x;
        int v = u + x;
        return v + ((v ^ x) / u >> 2);
    }

    Set<Dimension> getSelectedDimensionsFromBinaryString(String comboString) {
        Set<Dimension> dims = Sets.newHashSet();
        char[] charArray = comboString.toCharArray();
        for (int i = 0; i < charArray.length; i++) {
            if ((int) charArray[i] == (int) '1') {
                dims.add(getDimensions().get(i));
            }
        }
        return dims;
    }

}
