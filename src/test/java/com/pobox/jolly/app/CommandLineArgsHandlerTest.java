package com.pobox.jolly.app;

import com.google.common.collect.Lists;
import joptsimple.OptionParser;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;

class CommandLineArgsHandlerTest {
    private CommandLineArgsHandler clah;

    @BeforeEach
    void onSetup() {
        clah = new CommandLineArgsHandler(
                new OptionParser(),
                new MyHelpFormatter(),
                new IntegerValueConverter()
        );
    }

    @Test
    void testArgsN() {
        clah.parse("-n3", "2", "3", "8");
        assertThat(clah.getNTuple(), is(3));
    }

    @Test
    void testArgsDefaultN() {
        clah.parse("2", "3");
        assertThat(clah.getNTuple(), is(2));
        assertThat(clah.getDimensionFeatureCounts()
                       .size(), is(2));
    }

    @Test
    void testWithoutsArgs() {
        String[] argv = {"-n3", "2", "3", "-w1a2bc3b", "-w1b3a", "8", "3", "2", "2", "5", "3", "2", "2"};
        clah.parse(argv);
        assertThat(clah.getWithouts(), contains("1a2bc3b", "1b3a"));
        assertThat(clah.getWithouts(), hasSize(2));
    }

    @Test
    void testDimensions() {
        String[] argv = {"2", "3", "8", "3", "2", "2", "5", "3", "2", "2"};
        clah.parse(argv);
        ArrayList<Integer> expected = Lists.newArrayList(2, 3, 8, 3, 2, 2, 5, 3, 2, 2);
        for (int i = 0; i < 9; i++) {
            Integer actual = clah.getDimensionFeatureCounts()
                                 .get(i);
            Integer wanted = expected.get(i);
            assertThat("List index item: " + i + " incorrect, expected: " + wanted + " found: " + actual, actual,
                    is(wanted)
            );
        }
    }

    @Test
    void testSeed() {
        String[] argv = {"2", "3", "-s12345"};
        clah.parse(argv);
        assertThat(clah.getSeed(), is(12345));
    }

    @Test
    void testArgsLots() {
        String[] argv = {"-n3", "2", "3", "-w1a2bc3b", "-w1b3a", "-w1a4b", "-w9a10b", "-w3a4b", "-s3", "8", "3", "2",
                "2", "5", "3", "2", "2"};
        clah.parse(argv);
        assertThat(clah.getNTuple(), is(3));
        assertThat(clah.getDimensionFeatureCounts(), hasSize(10));
        assertThat(clah.getSeed(), is(3));
        assertThat(clah.getWithouts(), hasSize(5));
    }
}
