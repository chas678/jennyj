package com.pobox.jolly.app;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.startsWith;

class MyHelpFormatterTest {
    MyHelpFormatter sut;

    @BeforeEach
    void setUp() {
        sut = new MyHelpFormatter();
    }

    @Test
    void testFormat() {
        String result = sut.format(null);
        assertThat(result, startsWith("jennyJ:"));
    }
}
