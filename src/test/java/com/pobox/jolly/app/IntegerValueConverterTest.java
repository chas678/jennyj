package com.pobox.jolly.app;

import joptsimple.ValueConversionException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.jupiter.api.Assertions.assertThrows;

class IntegerValueConverterTest {

    IntegerValueConverter sut;

    @BeforeEach
    void setUp() {
        sut = new IntegerValueConverter();
    }

    @Test
    void testConvert() {
        Integer result = sut.convert("12345");
        assertThat(result, is(12345));
    }

    @Test
    void testConvertNegativeFail() {
        ValueConversionException ex = assertThrows(ValueConversionException.class, () -> sut.convert("-12"));
        assertThat(ex.getCause(), instanceOf(NumberFormatException.class));
    }

    @Test
    void testConvertFloat() {
        ValueConversionException ex = assertThrows(ValueConversionException.class, () -> sut.convert("1.2"));
        assertThat(ex.getCause(), instanceOf(NumberFormatException.class));
    }

    @Test
    void testConvertTooLargeFails() {
        //noinspection NumericOverflow
        ValueConversionException ex = assertThrows(ValueConversionException.class, () -> sut.convert(String.valueOf(Integer.MAX_VALUE + 1)));
        assertThat(ex.getCause(), instanceOf(NumberFormatException.class));
    }

    @Test
    void testValueType() {
        assertThat(sut.valueType(), instanceOf(Class.class));
    }

    @Test
    void testValuePattern() {
        assertThat(sut.valuePattern(), is(nullValue()));
    }
}
