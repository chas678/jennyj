package com.pobox.jolly.scoring;

import com.google.common.collect.ImmutableList;
import com.pobox.jolly.domain.TestPlan;
import org.junit.jupiter.api.Test;
import org.optaplanner.core.api.score.Score;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;


class TestPlanSimpleScoreCalculatorTest {
    TestPlanSimpleScoreCalculator sut;
    TestPlan testPlan;


    @Test
    void shouldTestCalculateScore() {
        sut = new TestPlanSimpleScoreCalculator();
        testPlan = new TestPlan(2, ImmutableList.of(3, 3, 3));
        Score score = sut.calculateScore(testPlan);
        assertThat(score.toLevelNumbers(), is(new Number[]{0, -27}));
    }
}
