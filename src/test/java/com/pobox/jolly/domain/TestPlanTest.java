package com.pobox.jolly.domain;

import com.google.common.collect.ImmutableList;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Set;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;


class TestPlanTest {
    TestPlan sut;

    @BeforeEach
    void onSetup() {
        sut = new TestPlan(2, ImmutableList.of(5, 6, 7, 8));
    }

    @Test
    void shouldTestGenerateAllPossibleTestcases() {
        Set<TestCase> result = sut.computeAllPossibleTestCases();
        assertThat(result, hasSize(1680));
        assertThat(result.iterator()
                         .next()
                         .getFeatures(), hasSize(4));
    }

}
