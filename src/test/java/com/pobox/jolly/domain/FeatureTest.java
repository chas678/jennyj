package com.pobox.jolly.domain;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

class FeatureTest {
    Feature sut;

    @BeforeEach
    void setUp() {
        sut = new Feature('z', 13);
    }

    @Test
    void testGetDimension() {
        assertThat(sut.dimension(), is('z'));
    }

    @Test
    void testGetFeature() {
        assertThat(sut.featureItem(), is(13));
    }

    @Test
    void testToString() {
        assertThat(sut.toString(), is("13z"));
    }

    @Test
    void testEquals() {
        assertThat(sut, is(new Feature('z', 13)));
    }

    @Test
    void testHashcode() {
        assertThat(sut.hashCode(), is(new Feature('z', 13).hashCode()));
    }

}
