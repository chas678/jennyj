package com.pobox.jolly.service;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.pobox.jolly.domain.Dimension;
import com.pobox.jolly.domain.Feature;
import com.pobox.jolly.domain.Tuple;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Set;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.jupiter.api.Assertions.assertThrows;

@Slf4j
class TupleGeneratorTest {
    TupleGenerator sut;
    List<Integer> inputFeatures;

    @Test
    void shouldTestGenerateDimensions() {
        inputFeatures = Lists.newArrayList(3, 3, 3);
        sut = TupleGenerator.builder()
                            .nTuples(3)
                            .domainsFeatureCount(inputFeatures)
                            .build();
        List<Dimension> result = sut.generateDimensions();

        assertThat(result, hasSize(3));

        assertThat(result.get(0)
                         .getFeatures(), hasSize(3));
        assertThat(result.get(1)
                         .getFeatures(), hasSize(3));
        assertThat(result.get(2)
                         .getFeatures(), hasSize(3));

        assertThat(result.get(0)
                         .getId(), is('a'));
        assertThat(result.get(1)
                         .getId(), is('b'));
        assertThat(result.get(2)
                         .getId(), is('c'));

        assertThat(result.get(0)
                         .getFeatures()
                         .get(0)
                         .dimension(), is('a'));
        assertThat(result.get(0)
                         .getFeatures()
                         .get(0)
                         .featureItem(), is(1));

        assertThat(result.get(1)
                         .getFeatures()
                         .get(1)
                         .dimension(), is('b'));
        assertThat(result.get(1)
                         .getFeatures()
                         .get(1)
                         .featureItem(), is(2));

        assertThat(result.get(2)
                         .getFeatures()
                         .get(2)
                         .dimension(), is('c'));
        assertThat(result.get(2)
                         .getFeatures()
                         .get(2)
                         .featureItem(), is(3));
    }

    @Test
    void shouldTestGenerateDimensionsMaxDimensions() {
        inputFeatures = Lists.newArrayList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
                11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
                21, 22, 23, 24, 25, 26, 27, 28, 29, 30,
                31, 32, 33, 34, 35, 36, 37, 38, 39, 40,
                41, 42, 43, 44, 45, 46, 47, 48, 49, 50,
                51, 52
        );
        sut = TupleGenerator.builder()
                            .nTuples(3)
                            .domainsFeatureCount(inputFeatures)
                            .build();
        List<Dimension> result = sut.generateDimensions();
        assertThat(result, hasSize(52));
        assertThat(result.get(0)
                         .getFeatures(), hasSize(1));
        assertThat(result.get(51)
                         .getFeatures(), hasSize(52));
        assertThat(result.get(0)
                         .getId(), is('a'));
        assertThat(result.get(51)
                         .getId(), is('Z'));
    }

    @Test
    void testConstructorBarfsWithBadNTuplesInputValue() {
        IllegalArgumentException ex = assertThrows(
                IllegalArgumentException.class,
                () -> TupleGenerator.builder()
                                    .nTuples(-1)
                                    .domainsFeatureCount(Lists.newArrayList(3, 3, 3))
                                    .build()
        );
        assertThat(ex.getMessage(), is("nTuples must be >0 and <= number of domains"));
    }

    @Test
    void testConstructorBarfsWithTooManyBadNTuplesInputValue() {

        IllegalArgumentException ex = assertThrows(
                IllegalArgumentException.class,
                () -> TupleGenerator.builder()
                                    .nTuples(99)
                                    .domainsFeatureCount(Lists.newArrayList(3, 3, 3))
                                    .build()
        );
        assertThat(ex.getMessage(), is("nTuples must be >0 and <= number of domains"));
    }

    @Test
    void testConstructorBarfsIfNullSentForDimensionsFeatureCountList() {
        NullPointerException ex = assertThrows(
                NullPointerException.class,
                () -> TupleGenerator.builder()
                                    .nTuples(3)
                                    .domainsFeatureCount(null)
                                    .build()
        );
        assertThat(ex.getMessage(), is("domainsFeatureCount cannot be null"));

    }

    @Test
    void testgetDimensionsLazyInstantiated() {
        sut = TupleGenerator.builder()
                            .nTuples(3)
                            .domainsFeatureCount(Lists.newArrayList(3, 3, 3))
                            .build();
        ImmutableList<Dimension> first = sut.getDimensions();
        ImmutableList<Dimension> second = sut.getDimensions();
        assertThat(first, is(sameInstance(second)));
    }

    @Test
    void testCreateCombinationTuples() {
        Feature a1 = new Feature('a', 1);
        Feature a2 = new Feature('a', 2);
        Feature a3 = new Feature('a', 3);
        Feature b1 = new Feature('b', 1);
        Feature b2 = new Feature('b', 2);
        Feature b3 = new Feature('b', 3);
        Feature c1 = new Feature('c', 1);
        Feature c2 = new Feature('c', 2);
        Feature c3 = new Feature('c', 3);

        Tuple a = new Tuple(ImmutableSet.of(a1, b1, c1));
        Tuple b = new Tuple(ImmutableSet.of(a1, b1, c2));
        Tuple c = new Tuple(ImmutableSet.of(a1, b1, c3));
        Tuple d = new Tuple(ImmutableSet.of(a2, b1, c1));
        Tuple e = new Tuple(ImmutableSet.of(a2, b1, c2));
        Tuple f = new Tuple(ImmutableSet.of(a2, b1, c3));
        Tuple g = new Tuple(ImmutableSet.of(a3, b1, c1));
        Tuple h = new Tuple(ImmutableSet.of(a3, b1, c2));
        Tuple i = new Tuple(ImmutableSet.of(a3, b1, c3));

        Tuple j = new Tuple(ImmutableSet.of(a1, b2, c1));
        Tuple k = new Tuple(ImmutableSet.of(a1, b2, c2));
        Tuple l = new Tuple(ImmutableSet.of(a1, b2, c3));
        Tuple m = new Tuple(ImmutableSet.of(a2, b2, c1));
        Tuple n = new Tuple(ImmutableSet.of(a2, b2, c2));
        Tuple o = new Tuple(ImmutableSet.of(a2, b2, c3));
        Tuple p = new Tuple(ImmutableSet.of(a3, b2, c1));
        Tuple q = new Tuple(ImmutableSet.of(a3, b2, c2));
        Tuple r = new Tuple(ImmutableSet.of(a3, b2, c3));

        Tuple s = new Tuple(ImmutableSet.of(a1, b3, c1));
        Tuple t = new Tuple(ImmutableSet.of(a1, b3, c2));
        Tuple u = new Tuple(ImmutableSet.of(a1, b3, c3));
        Tuple v = new Tuple(ImmutableSet.of(a2, b3, c1));
        Tuple w = new Tuple(ImmutableSet.of(a2, b3, c2));
        Tuple x = new Tuple(ImmutableSet.of(a2, b3, c3));
        Tuple y = new Tuple(ImmutableSet.of(a3, b3, c1));
        Tuple z = new Tuple(ImmutableSet.of(a3, b3, c2));
        Tuple aa = new Tuple(ImmutableSet.of(a3, b3, c3));

        Set<Tuple> expected = Sets.newHashSet(a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v, w, x,
                y, z, aa
        );

        inputFeatures = Lists.newArrayList(3, 3, 3);
        sut = TupleGenerator.builder()
                            .nTuples(3)
                            .domainsFeatureCount(inputFeatures)
                            .build();
        Set<Dimension> input = ImmutableSet.copyOf(sut.getDimensions());
        Set<Tuple> actual = sut.createCombinationTuples(input);

        assertThat(actual, containsInAnyOrder(expected.toArray(new Tuple[0])));
    }

    @Test
    void testDimensionSelectN_2from3() {
        inputFeatures = Lists.newArrayList(3, 3, 3);
        sut = TupleGenerator.builder()
                            .nTuples(2)
                            .domainsFeatureCount(inputFeatures)
                            .build();
        Set<Set<Dimension>> actual = sut.select_N_Dimensions();
        assertThat(actual, hasSize(3));
    }

    @Test
    void testDimensionSelectN_3from3() {
        inputFeatures = Lists.newArrayList(3, 3, 3);
        sut = TupleGenerator.builder()
                            .nTuples(3)
                            .domainsFeatureCount(inputFeatures)
                            .build();
        Set<Set<Dimension>> actual = sut.select_N_Dimensions();
        assertThat(actual, hasSize(1));
    }

    @Test
    void testGenerateTuples() {
        int nTuples = 3;
        inputFeatures = Lists.newArrayList(3, 3, 3);
        sut = TupleGenerator.builder()
                            .nTuples(nTuples)
                            .domainsFeatureCount(inputFeatures)
                            .build();
        Set<Tuple> actual = sut.allRequiredTuples();
        log.debug("all tuples for n of: {} is {}", sut.getNTuples(), actual);
        assertThat(actual, hasSize(27));
        assertThat("only asked for n tuples", actual.iterator()
                                                    .next()
                                                    .getFeatures()
                                                    .size(), is(nTuples));
    }

    @Test
    void testSelect_N_Dimensions() {
        int nTuples = 2;
        inputFeatures = Lists.newArrayList(3, 3, 3);
        sut = TupleGenerator.builder()
                            .nTuples(nTuples)
                            .domainsFeatureCount(inputFeatures)
                            .build();

        Set<Set<Dimension>> sets = sut.select_N_Dimensions();
        assertThat(sets.size(), is(3));
        assertThat(sets.iterator()
                       .next()
                       .size(), is(2));

    }
}
