#JennyJ

BUILD: `mvn clean package`
EXECUTE: `java -jar jennyj-1.0.jar -n 2 2 3`

An [Optaplanner](http://optaplanner.org) based Pairwise test plan generator; derive a test plan with the
minimum number of test cases required to cover all pairs (or triples etc)
of features.  Optionally allow 'withouts' - features that cannot appear together in a testcase.

* Pairwise testing: examples and background see http://www.pairwise.org
* Interface and operation is designed to be functionally equivalent to the Jenny software at http://burtleburtle.net/bob/math/jenny.html
(Grateful thanks to Bob Jenkins for placing the original C based version of Jenny into the Public Domain)


## DOMAIN:
* TestPlan - a collection of testcases
* TestCase - a single test run that covers a number of features
* Feature - a testable feature
* Dimension - a list of possible tests/values for one Feature
* Tuple - n-Features ("n" is a number to denote pairs, triples, etc)
* Without - Describes Tuples that must not appear together in a testcase

## jenny.c help file

    jenny.c -- jennyrate tests from m dimensions of features that cover all
      n-tuples of features, n <= m, with each feature chosen from a different 
      dimension.  For example, given 10 dimensions (m=10) with 2 to 8 features
      apiece, cover all feature triplets (n=3).  A lower bound on the number
      of tests required is the product of the sizes of the largest n dimensions.
      Already-written tests can be piped in to be reused.
    
    Arguments
      Arguments without a leading '-' : an integer in 2..52.  Represents a
           dimension.  Dimensions are implicitly numbered 1..65535, in the 
           order they appear.  Features in dimensions are always implicitly
           given 1-character names, which are in order a..z, A..Z .  It's a
           good idea to pass the output of jenny through a postprocessor that
           expands these names into something intelligible.
    
      -o : old.  -ofoo.txt reads existing tests from the file foo.txt, includes
           those tests in the output, and adds whatever other tests are needed to
           complete coverage.  An error is reported if the input tests are of the
           wrong shape or contain disallowed feature interactions.  If you have
           added new dimensions since those tests were written, be sure to include
           a do-nothing feature in each new dimension, then pad the existing tests
           with do-nothing out to the correct number of dimensions.
    
      -h : help.  Print out instructions for using jenny.
    
      -n : an integer.  Cover all n-tuples of features, one from each dimension.
           Default is 2 (pairs).  3 (triplets) may be reasonable.  4 (quadruplets)
           is definitely overkill.  n > 4 is highly discouraged.
    
      -s : seed.  An integer.  Seed the random number generator.
    
      -w : without this combination of features.  A feature is given by a dimension
           number followed by a one-character feature name.  A single -w can
           disallow multiple features in a dimension.  For example, -w1a2cd4ac
           disallows the combinations (1a,2c,4a),(1a,2c,4c),(1a,2d,4a),(1a,2d,4c)
           where 1a represents the first dimension's first feature, 2c is the 
           second dimension's third feature, and 4a is the fourth dimension's
           first feature.
    
    Example: 10 dimensions of 2, 3, 8, 3, 2, 2, 5, 3, 2, 2 features apiece,
    with some restrictions, asking for all triplets of features to be covered.
    This will produce at least 8*5*3=120 tests.  Splitting the 8 features in the
    third dimension into three dimensions each of length 2 would reduce the
    number of testcases required to at least 5*3*3=45.
    
      jenny -n3 2 3 8 -w1a2bc3b -w1b3a 3 -w1a4b 2 2 5 3 2 2 -w9a10b -w3a4b -s3
      
